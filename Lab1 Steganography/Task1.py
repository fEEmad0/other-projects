# -*- coding: utf-8 -*-
import sys
import os
from PIL import Image
import PySimpleGUI as sG
import numpy as np
import random


class ImageFunc:
    @staticmethod
    def _get_image(path):
        try:
            img = Image.open(path)
        except IOError:
            return None
        except AttributeError:
            return None
        if img.mode != 'RGB':
            print('[!] Non supported image mode')
        return img

    @staticmethod
    def _get_text(path):
        try:
            with open(path, 'r', encoding='utf-8-sig') as file:
                text = file.read()
        except FileNotFoundError:
            return None
        return text

    @staticmethod
    def _save_image(img, path):
        try:
            img.save(path)
        except IOError as e:
            print(e)
            sys.exit()

    @staticmethod
    def _save_text(text, path):
        # print(text)
        with open(path, 'w', encoding='utf-8-sig') as file:
            (file.write(text))

    def _set_bit(self, bit: int) -> bool:
        if 0 <= bit <= 7:
            self.bit = bit
            return True
        else:
            return False

    def _set_palette(self, palette: str):
        color = {"R": 0, "G": 1, "B": 2}
        if palette in color.keys():
            self.palette = color[palette]
        else:
            raise KeyError('Данный ключ отсутствует')

    @staticmethod
    def _text_to_bits(text):
        bits = bin(int.from_bytes(text.encode(), 'big'))[2:]
        return bits[::-1].zfill(8 * ((len(bits) + 7) // 8))[::-1]

    @staticmethod
    def _text_from_bits(bits):
        text, count, symbol = '', 0, ''

        while count < len(bits):
            n = int(bits[count:count + 32], 2)
            try:
                symbol = n.to_bytes(4, "big").decode() or '\0'
            except UnicodeDecodeError:
                n = int(bits[count:count + 16], 2)

                try:
                    symbol = n.to_bytes(2, 'big').decode() or '\0'
                except UnicodeDecodeError:
                    n = int(bits[count:count + 8], 2)
                    try:
                        symbol = n.to_bytes(1, 'big').decode() or '\0'
                    except UnicodeDecodeError:
                        symbol = "?"
                    count += 8
                else:
                    count += 16
            else:
                count += 32
            text += symbol
        return text


class LSBEncode(ImageFunc):
    def __init__(self, path: str, palette: str, bit: int, outfile: str, text: str):
        self.image = self._get_image(path)
        if self.image is None:
            print("Путь к изображению указан неверно!")
            return

        self.outfile = outfile
        self.text = self._get_text(text)
        if self.text is None:
            print("Путь к тексту указан неверно!")
            return
        self._set_palette(palette)
        self._set_bit(bit)

        size = str(self.image).split()[3][5:].split('x')
        self.rows = int(size[0])
        self.cols = int(size[1])
        self.data = list(self.image.getdata())

        for i in range(len(self.data)):
            self.data[i] = list(self.data[i])

        self.bits = self._text_to_bits(self.text)

    def encode(self):
        for i in range(len(self.bits)):
            if self.data[i][self.palette] & (2 ** self.bit) != int(self.bits[i]):
                self.data[i][self.palette] = self.data[i][self.palette] ^ (2 ** self.bit)

        for i in range(len(self.data)):
            self.data[i] = tuple(self.data[i])
        self.image.putdata(self.data)
        self._save_image(self.image, self.outfile)

        return True


class LSBDecode(ImageFunc):
    def __init__(self, path: str, palette: str, bit: int, outfile: str):
        self.image = self._get_image(path)
        if self.image is None:
            return
        self.outfile = outfile
        self._set_palette(palette)
        self._set_bit(bit)

        size = str(self.image).split()[3][5:].split('x')
        self.rows = int(size[0])
        self.cols = int(size[1])
        self.data = list(self.image.getdata())

        for i in range(len(self.data)):
            self.data[i] = list(self.data[i])

    def decode(self):
        bits = ''
        for pixel in self.data:
            if pixel[self.palette] & 2 ** self.bit:
                bits += '1'
            else:
                bits += '0'

        text = self._text_from_bits(bits)
        self._save_text(text, self.outfile)

        return True


class PermutationEncode(ImageFunc):
    def __init__(self, path: str, palette: str, bit: int, outfile: str, text: str):
        self.image = self._get_image(path)
        if self.image is None:
            print("Путь к изображению указан неверно!")
            return
        self.outfile = outfile
        self.text = self._get_text(text)
        if self.text is None:
            print("Путь к тексту указан неверно!")
            return
        self._set_palette(palette)
        self._set_bit(bit)

        size = str(self.image).split()[3][5:].split('x')
        self.rows = int(size[0])
        self.cols = int(size[1])
        self.data = list(self.image.getdata())

        for i in range(len(self.data)):
            self.data[i] = list(self.data[i])

        self.bits = self._text_to_bits(self.text)

        self.permutation_matrix = np.eye(10)
        for i in range(len(self.permutation_matrix)):
            current = random.randint(i, len(self.permutation_matrix) - 1)
            self.permutation_matrix[[i, current]] = \
                self.permutation_matrix[[current, i]]

    def encode(self):
        current_pos = 0
        bits = ''
        for _ in range(len(self.bits) // len(self.permutation_matrix)):
            bits += ''.join(str(np.array(list(self.bits[current_pos:current_pos + len(self.permutation_matrix):]))
                                .astype(float).dot(self.permutation_matrix).astype(int))[1:-1].split())
            current_pos += len(self.permutation_matrix)

        bits += self.bits[(len(self.bits) // len(self.permutation_matrix)) *
                          len(self.permutation_matrix):]

        for i in range(len(bits)):
            if self.data[i][self.palette] & (2 ** self.bit) != int(bits[i]):
                self.data[i][self.palette] = self.data[i][self.palette] ^ (2 ** self.bit)

        for i in range(len(self.data)):
            self.data[i] = tuple(self.data[i])
        self.image.putdata(self.data)
        self._save_image(self.image, self.outfile)

        return self.permutation_matrix


class PermutationDecode(ImageFunc):
    def __init__(self, path: str, palette: str, bit: int, outfile: str, permutation_matrix: np.ndarray):
        self.image = self._get_image(path)
        if self.image is None:
            return
        self.outfile = outfile
        self._set_palette(palette)
        self._set_bit(bit)

        self.permutation_matrix = permutation_matrix

        size = str(self.image).split()[3][5:].split('x')
        self.rows = int(size[0])
        self.cols = int(size[1])
        self.data = list(self.image.getdata())

        for i in range(len(self.data)):
            self.data[i] = list(self.data[i])

    def decode(self):
        bits = ''
        for pixel in self.data:
            if pixel[self.palette] & 2 ** self.bit:
                bits += '1'
            else:
                bits += '0'

        current_pos = 0
        another_bits = ''
        for _ in range(len(bits) // len(self.permutation_matrix)):
            another_bits += ''.join(str(np.array(list(bits[current_pos:current_pos + len(self.permutation_matrix):]))
                                        .astype(float).dot(self.permutation_matrix.transpose())
                                        .astype(int))[1:-1].split())
            current_pos += len(self.permutation_matrix)

        another_bits += bits[(len(bits) // len(self.permutation_matrix)) *
                             len(self.permutation_matrix):]

        text = self._text_from_bits(another_bits)
        self._save_text(text, self.outfile)

        return True


class PseudoRandomEncode(ImageFunc):
    def __init__(self, path: str, palette: str, bit: int, outfile: str, text: str):
        self.image = self._get_image(path)
        if self.image is None:
            print("Путь к изображению указан неверно!")
            return
        self.outfile = outfile
        self.text = self._get_text(text)
        if self.text is None:
            print("Путь к тексту указан неверно!")
            return
        self._set_palette(palette)
        self._set_bit(bit)

        size = str(self.image).split()[3][5:].split('x')
        self.rows = int(size[0])
        self.cols = int(size[1])
        self.data = list(self.image.getdata())

        for i in range(len(self.data)):
            self.data[i] = list(self.data[i])

        self.bits = self._text_to_bits(self.text)

        self.key = [random.randint(1, 15) for _ in range(0, 100)]

    def encode(self):
        current, i = self.key[0], 0
        while current <= len(self.data) - 1 and i != len(self.bits):
            if self.data[current][self.palette] & (2 ** self.bit) != int(self.bits[i]):
                self.data[current][self.palette] = self.data[current][self.palette] ^ (2 ** self.bit)
            i += 1
            current += self.key[i % len(self.key) - 1]

        for i in range(len(self.data)):
            self.data[i] = tuple(self.data[i])
        self.image.putdata(self.data)
        self._save_image(self.image, self.outfile)

        return self.key


class PseudoRandomDecode(ImageFunc):
    def __init__(self, path: str, palette: str, bit: int, outfile: str, key: list):
        self.image = self._get_image(path)
        if self.image is None:
            return
        self.outfile = outfile
        self._set_palette(palette)
        self._set_bit(bit)

        self.key = key

        size = str(self.image).split()[3][5:].split('x')
        self.rows = int(size[0])
        self.cols = int(size[1])
        self.data = list(self.image.getdata())

        for i in range(len(self.data)):
            self.data[i] = list(self.data[i])

    def decode(self):
        bits, current, i = '', self.key[0], 0

        while current <= len(self.data) - 1:
            if self.data[current][self.palette] & 2 ** self.bit:
                bits += '1'
            else:
                bits += '0'
            i += 1
            current += self.key[i % len(self.key) - 1]

        text = self._text_from_bits(bits)
        self._save_text(text, self.outfile)

        return True


class Frame:
    def separator(self):
        while True:
            yield print('==========================================')

    choices = ("LSB", "Псевдослучайная перестановка", "Псевдослучайный интервал")
    palette, cur_pal = {2: "R", 3: "G", 4: "B"}, "B"
    layout = [
        [sG.Text('Исходное\nизображение'), sG.InputText(''),
         sG.FileBrowse('Открыть..', file_types=(("Image Files", "*.bmp"), ("Image Files", "*.jpg"),
                                                ("Image Files", "*.jpeg")))],
        [sG.Text('Исходный    \nтекст   '), sG.InputText(''),
         sG.FileBrowse('Открыть..', file_types=(("Text Files", "*.txt"),))],
        [sG.Radio('R', "PALETTE", enable_events=True), sG.Radio('G', "PALETTE", enable_events=True),
            sG.Radio('B', "PALETTE", default=True, enable_events=True)],
        [sG.Output(size=(70, 15))],
        [sG.Combo(choices, size=(30, 1), tooltip="Нажмите на поле для выбора")],
        [sG.Submit("Подтвердить"), sG.Cancel("Выйти")]
    ]
    window = sG.Window('Лабораторная работа №1', layout, element_justification='c')
    sep = separator(None)
    while True:
        event, values = window.read()
        if event in (2, 3, 4):
            cur_pal = palette[event]
            print(cur_pal)
        elif event in (None, 'Выйти', 'Cancel'):
            break
        elif event in 'Подтвердить':
            if values[5] == "LSB":
                print("LSB")
                outfile = os.getcwd() + "/LSB.bmp"
                try:
                    if LSBEncode(values[0], cur_pal, 0, outfile, values[1]).encode():
                        print("Встраивание прошло успешно")
                    else:
                        break

                    outfile_2 = os.getcwd() + "/LSB.txt"
                    if LSBDecode(outfile, cur_pal, 0, outfile_2).decode():
                        print("Извлечение прошло успешно")
                except AttributeError:
                    pass

            elif values[5] == "Псевдослучайная перестановка":
                print("Псевдослучайная перестановка")
                outfile = os.getcwd() + "/Permutation.bmp"

                try:
                    permutation_matrix = PermutationEncode(values[0], cur_pal, 0, outfile, values[1]).encode()
                    if type(permutation_matrix) is np.ndarray:
                        print("Встраивание прошло успешно")
                        print(*permutation_matrix, sep="\n")
                    else:
                        break

                    outfile_2 = os.getcwd() + "/Permutation.txt"
                    if PermutationDecode(outfile, cur_pal, 0, outfile_2, permutation_matrix).decode():
                        print("Извлечение прошло успешно")
                except AttributeError:
                    pass

            elif values[5] == "Псевдослучайный интервал":
                print("Псевдослучайный интервал")
                outfile = os.getcwd() + "/Pseudorandom.bmp"

                try:
                    key = PseudoRandomEncode(values[0], cur_pal, 0, outfile, values[1]).encode()
                    if type(key) is list:
                        print("Встраивание прошло успешно")
                        print(*key)
                    else:
                        break

                    outfile_2 = os.getcwd() + "/Pseudorandom.txt"
                    if PseudoRandomDecode(outfile, cur_pal, 0, outfile_2, key).decode():
                        print("Извлечение прошло успешно")
                except AttributeError:
                    pass

            else:
                print("Выберите способ встраивания")
            next(sep)
