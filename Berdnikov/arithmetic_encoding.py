import random
import PySimpleGUI as sg


def encoding(ranges: dict, current_letter: str) -> float:
    print("Encoding...")
    # current_letter = input("Input your word: ")
    low_old, high_old = ranges[current_letter[0]][0], \
                        ranges[current_letter[0]][1]
    print(low_old, high_old)
    for i in range(1, len(current_letter) - 1):
        high = low_old + ranges[current_letter[i]][1] * (high_old - low_old)
        low = low_old + ranges[current_letter[i]][0] * (high_old - low_old)
        high_old, low_old = high, low
        print(low_old, high_old)
    return float("{:.6}".format(random.uniform(low_old, high_old)))


def decoding(code_num: float, ranges: dict, i: int) -> str:
    print("Decoding...")
    res, current_letter = "", ""
    while i != 0:

        for k in ranges:
            if code_num < ranges[k][1]:
                current_letter = k
                break
        res += current_letter

        code_num = (code_num - ranges[current_letter][0]) / \
                   (ranges[current_letter][1] - ranges[current_letter][0])
        print(code_num)

        i -= 1
    return res


if __name__ == '__main__':
    ranges, code_num = {}, 0
    layout = [
        [sg.Text('Буква'), sg.InputText('', size=(5, 1)), sg.Text('Промежуток'),
         sg.InputText('', size=(15, 1)), sg.Submit('Добавить')],
        [sg.Output(size=(55, 15))],
        [sg.InputText('Слово для кодирования', size=(22, 1)), sg.Submit("Кодировать")],
        [sg.InputText('Кол-во букв в слове', size=(18, 1)), sg.Submit("Декодировать"), sg.Cancel("Выйти")]
    ]
    window = sg.Window('Парсер блоков', layout)
    count = 0
    while True:  # The Event Loop
        event, values = window.read()
        # print(event) #debug
        if event in (None, 'Выйти', 'Cancel'):
            break
        elif event in 'Добавить':
            key = values[0]
            values[1] = values[1].split('-')
            for i in range(len(values[1])):
                values[1][i] = float(values[1][i])
            ranges.setdefault(key, values[1])
            print(key, " ", ranges.get(key))
            count += 1
        elif event in 'Кодировать':
            print(values[2])
            code_num = encoding(ranges, values[2])
            print("Answer is ", code_num)
        elif event in 'Декодировать':
            print(int(values[3]))
            print(decoding(code_num, ranges, int(values[3])))
