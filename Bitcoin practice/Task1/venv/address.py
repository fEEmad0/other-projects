import os, binascii, hashlib, base58, ecdsa


def ripemd160(x):
    d = hashlib.new('ripemd160')
    d.update(x)
    return d


for n in range(1):  # number of key pairs to generate`

    # generate private key , uncompressed WIF starts with "5"
    # генерируем случайное значение
    priv_key = os.urandom(32)

    # переводим в формат Wallet Import Format
    fullkey = '80' + binascii.hexlify(priv_key).decode()
    sha256a = hashlib.sha256(binascii.unhexlify(fullkey)).hexdigest()
    sha256b = hashlib.sha256(binascii.unhexlify(sha256a)).hexdigest()
    WIF = base58.b58encode(binascii.unhexlify(fullkey + sha256b[:8]))

    # получаем публичный не сжатый ключ, он начинается с единицы
    sk = ecdsa.SigningKey.from_string(priv_key, curve=ecdsa.SECP256k1)
    vk = sk.get_verifying_key()
    publ_key = '04' + binascii.hexlify(vk.to_string()).decode()

    # формируем адресс кошелька для основной сети 0х00
    hash160 = ripemd160(hashlib.sha256(binascii.unhexlify(publ_key)).digest()).digest()
    publ_addr_a = b"\x00" + hash160

    # высчитываем контрольную сумму и
    checksum = hashlib.sha256(hashlib.sha256(publ_addr_a).digest()).digest()[:4]

    # добавляем её первые 4 байта в конец адреса приводя его к формату Base58
    publ_addr_b = base58.b58encode(publ_addr_a + checksum)

    i = n + 1
    print('Private Key    ', str(i) + ": " + WIF.decode())
    print("Bitcoin Address", str(i) + ": " + publ_addr_b.decode(), "\n")
