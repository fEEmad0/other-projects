import os
import datetime
import hashlib
import PySimpleGUI as sg


# Необходимо для записи чисел в формате big-endian
def reverse(input):
    L = len(input)
    if (L % 2) != 0:
        return None
    else:
        Res = ''
        L = L // 2
        for i in range(L):
            T = input[i * 2] + input[i * 2 + 1]
            Res = T + Res
            T = ''
        return (Res)


# Нужно для сравнения значений хэша дерева Меркля
def merkle_root(lst):
    sha256d = lambda x: hashlib.sha256(hashlib.sha256(x).digest()).digest()
    hash_pair = lambda x, y: sha256d(x[::-1] + y[::-1])[::-1]
    if len(lst) == 1: return lst[0]
    if len(lst) % 2 == 1:
        lst.append(lst[-1])
    return merkle_root([hash_pair(x, y) for x, y in zip(*[iter(lst)] * 2)])


def read_bytes(file, n, byte_order='L'):
    data = file.read(n)
    if byte_order == 'L':
        data = data[::-1]
    data = data.hex().upper()
    return data


def read_var_int(file):
    b = file.read(1)
    bInt = int(b.hex(), 16)
    c = 0
    data = ''
    if bInt < 253:
        c = 1
        data = b.hex().upper()
    if bInt == 253: c = 3
    if bInt == 254: c = 5
    if bInt == 255: c = 9
    for j in range(1, c):
        b = file.read(1)
        b = b.hex().upper()
        data = b + data
    return data


def parse(path_1: str, path_2: str):
    # Указываем директории для считывания файлов-блоков и записи распаршеных данных
    # dirA = os.getcwd() + '/'
    # dirB = os.getcwd() + '/'
    if len(path_2) == 0:
        print("Не указан путь сохранения файлов")
        return

    dirA = path_1 + '/'
    dirB = path_2 + '/'
    print(path_1, path_2, sep='\n')

    # Считываем информацию о файлах в директории и отбираем только с расширением .dat
    # и началом blk
    fList = os.listdir(dirA)
    fList = [x for x in fList if (x.endswith('.dat') and x.startswith('blk'))]

    if len(fList) == 0:
        print("В папке нет блоков")
        return

    fList.sort()

    for i in fList:
        # Берём название файла и создаём аналогичный файл в формате .txt
        nameSrc = i
        nameRes = nameSrc.replace('.dat', '.txt')
        resList = []
        a = 0
        t = dirA + nameSrc

        # Информация о названии блока и времени его парсинга
        resList.append('Start ' + t + ' in ' + str(datetime.datetime.now()))
        print('Start ' + t + ' in ' + str(datetime.datetime.now()))

        # Открываем файл для чтения в двоичном режиме
        f = open(t, 'rb')
        hex_var = ''

        # Берём размер файла и идём в цикле
        # пока значение указателя не уравняется с размером
        fSize = os.path.getsize(t)
        while f.tell() != fSize:

            hex_var = read_bytes(f, 4)
            resList.append('Magic number = ' + hex_var)

            hex_var = read_bytes(f, 4)
            resList.append('Block size = ' + hex_var)

            # Читаем заголовок и разбираем его на составляющие
            tmpPos3 = f.tell()
            hex_var = read_bytes(f, 80, 'B')
            hex_var = bytes.fromhex(hex_var)
            hex_var = hashlib.new('sha256', hex_var).digest()
            hex_var = hashlib.new('sha256', hex_var).digest()
            hex_var = hex_var[::-1]
            hex_var = hex_var.hex().upper()
            resList.append('SHA256 hash of the current block hash = ' + hex_var)

            f.seek(tmpPos3, 0)
            hex_var = read_bytes(f, 4)
            resList.append('Version number = ' + hex_var)
            hex_var = read_bytes(f, 32)
            resList.append('SHA256 hash of the previous block hash = ' + hex_var)
            hex_var = read_bytes(f, 32)
            resList.append('MerkleRoot hash = ' + hex_var)
            MerkleRoot = hex_var
            hex_var = read_bytes(f, 4)
            resList.append('Time stamp = ' + hex_var)
            hex_var = read_bytes(f, 4)
            resList.append('Difficulty = ' + hex_var)
            hex_var = read_bytes(f, 4)
            resList.append('Random number = ' + hex_var)

            # Считываем информацию о транзакциях и их количестве
            hex_var = read_var_int(f)
            txCount = int(hex_var, 16)
            resList.append('Transactions count = ' + str(txCount))
            resList.append('')
            hex_var = ''
            RawTX = ''
            tx_hashes = []

            # Разбираем транзакции на составляющие ровно количество_транзакций раз
            for k in range(txCount):
                hex_var = read_bytes(f, 4)
                resList.append('TX version number = ' + hex_var)
                RawTX = reverse(hex_var)
                hex_var = ''
                witness = False
                b = f.read(1)
                tmpB = b.hex().upper()
                bInt = int(b.hex(), 16)
                if bInt == 0:
                    tmpB = ''
                    f.seek(1, 1)
                    c = 0
                    c = f.read(1)
                    bInt = int(c.hex(), 16)
                    tmpB = c.hex().upper()
                    witness = True
                c = 0
                if bInt < 253:
                    c = 1
                    hex_var = hex(bInt)[2:].upper().zfill(2)
                    tmpB = ''
                if bInt == 253: c = 3
                if bInt == 254: c = 5
                if bInt == 255: c = 9
                for j in range(1, c):
                    b = f.read(1)
                    b = b.hex().upper()
                    hex_var = b + hex_var
                inCount = int(hex_var, 16)
                resList.append('Inputs count = ' + hex_var)
                hex_var = hex_var + tmpB
                RawTX = RawTX + reverse(hex_var)
                for m in range(inCount):
                    hex_var = read_bytes(f, 32)
                    resList.append('TX from hash = ' + hex_var)
                    RawTX = RawTX + reverse(hex_var)
                    hex_var = read_bytes(f, 4)
                    resList.append('N output = ' + hex_var)
                    RawTX = RawTX + reverse(hex_var)
                    hex_var = ''
                    b = f.read(1)
                    tmpB = b.hex().upper()
                    bInt = int(b.hex(), 16)
                    c = 0
                    if bInt < 253:
                        c = 1
                        hex_var = b.hex().upper()
                        tmpB = ''
                    if bInt == 253: c = 3
                    if bInt == 254: c = 5
                    if bInt == 255: c = 9
                    for j in range(1, c):
                        b = f.read(1)
                        b = b.hex().upper()
                        hex_var = b + hex_var
                    scriptLength = int(hex_var, 16)
                    hex_var = hex_var + tmpB
                    RawTX = RawTX + reverse(hex_var)
                    hex_var = read_bytes(f, scriptLength, 'B')
                    resList.append('Input script = ' + hex_var)
                    RawTX = RawTX + hex_var
                    hex_var = read_bytes(f, 4, 'B')
                    resList.append('Sequence number = ' + hex_var)
                    RawTX = RawTX + hex_var
                    hex_var = ''
                b = f.read(1)
                tmpB = b.hex().upper()
                bInt = int(b.hex(), 16)
                c = 0
                if bInt < 253:
                    c = 1
                    hex_var = b.hex().upper()
                    tmpB = ''
                if bInt == 253: c = 3
                if bInt == 254: c = 5
                if bInt == 255: c = 9
                for j in range(1, c):
                    b = f.read(1)
                    b = b.hex().upper()
                    hex_var = b + hex_var
                outputCount = int(hex_var, 16)
                hex_var = hex_var + tmpB
                resList.append('Outputs count = ' + str(outputCount))
                RawTX = RawTX + reverse(hex_var)
                for m in range(outputCount):
                    hex_var = read_bytes(f, 8)
                    Value = hex_var
                    RawTX = RawTX + reverse(hex_var)
                    hex_var = ''
                    b = f.read(1)
                    tmpB = b.hex().upper()
                    bInt = int(b.hex(), 16)
                    c = 0
                    if bInt < 253:
                        c = 1
                        hex_var = b.hex().upper()
                        tmpB = ''
                    if bInt == 253: c = 3
                    if bInt == 254: c = 5
                    if bInt == 255: c = 9
                    for j in range(1, c):
                        b = f.read(1)
                        b = b.hex().upper()
                        hex_var = b + hex_var
                    scriptLength = int(hex_var, 16)
                    hex_var = hex_var + tmpB
                    RawTX = RawTX + reverse(hex_var)
                    hex_var = read_bytes(f, scriptLength, 'B')
                    resList.append('Value = ' + Value)
                    resList.append('Output script = ' + hex_var)
                    RawTX = RawTX + hex_var
                    hex_var = ''
                if witness == True:
                    for m in range(inCount):
                        hex_var = read_varint(f)
                        WitnessLength = int(hex_var, 16)
                        for j in range(WitnessLength):
                            hex_var = read_varint(f)
                            WitnessItemLength = int(hex_var, 16)
                            hex_var = read_bytes(f, WitnessItemLength)
                            resList.append(
                                'Witness ' + str(m) + ' ' + str(j) + ' ' + str(WitnessItemLength) + ' ' + hex_var)
                            hex_var = ''
                witness = False
                hex_var = read_bytes(f, 4)

                resList.append('Lock time = ' + hex_var)
                RawTX = RawTX + reverse(hex_var)
                hex_var = RawTX
                hex_var = bytes.fromhex(hex_var)
                hex_var = hashlib.new('sha256', hex_var).digest()
                hex_var = hashlib.new('sha256', hex_var).digest()
                hex_var = hex_var[::-1]
                hex_var = hex_var.hex().upper()
                resList.append('TX hash = ' + hex_var)
                tx_hashes.append(hex_var)
                resList.append('')
                hex_var = ''
                RawTX = ''

            a += 1
            tx_hashes = [bytes.fromhex(h) for h in tx_hashes]
            hex_var = merkle_root(tx_hashes).hex().upper()
            if hex_var != MerkleRoot:
                print('Merkle roots does not match! >', MerkleRoot, hex_var)

        f.close()
        f = open(dirB + nameRes, 'w')
        for j in resList:
            f.write(j + '\n')
        f.close()
    return 0


if __name__ == '__main__':
    layout = [
        [sg.Text('Откуда'), sg.InputText(''), sg.FolderBrowse('Открыть..')],
        [sg.Text('Куда   '), sg.InputText(''), sg.FolderBrowse('Открыть..')],
        [sg.Output(size=(70, 15))],
        [sg.Submit("Подтвердить"), sg.Cancel("Выйти")]
    ]
    window = sg.Window('Парсер блоков', layout)
    while True:  # The Event Loop
        event, values = window.read()
        # print(event) #debug
        if event in (None, 'Выйти', 'Cancel'):
            break
        elif event in 'Подтвердить':
            parse(values[0], values[1])
