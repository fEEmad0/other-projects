# Other projects on Python that created by myself
## Berdnikov
Applications for solving arithmetic coding problems with a simple graphical interface
## Bitcoin practice
The Task 1 folder contains two applications that I adapted for:
- creating a wallet address in different formats
- Parsing early Bitcoin blocks
## Lab1 Steganography
Contains a Python implementation of a laboratory work on the subject of Steganography, (practically) adapted for the end user, i.e. containing a graphical interface and in the final version (the one that was sent to the teacher) consisting only of a file with the extension .exe
## Twitter for Oleinikov
A small application showing how to use OAuth technology and the tweepy Twitter library
